#include <iostream>
#include "Graphic.h"
#include <SFML/Graphics.hpp>

int main()
{
  std::shared_ptr<Board> chart = std::make_shared<Board>();

  std::unique_ptr<Graphic> graph = std::make_unique<Graphic>();

  chart->Fill();

  graph->setHolder(chart);

  for (int i = 0; i < 8; ++i)
  {
    for (int j = 0; j < 8; ++j)
    {
      if ((*chart)[j][i] == nullptr) std::cout << '0';
      else std::cout << (*chart)[j][i]->getColor();
    }
    std::cout << std::endl;
  }

  graph->runWindow();

  return 0;
}