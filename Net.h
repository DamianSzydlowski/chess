//
// Created by xarti on 01.04.19.
//

#ifndef CHESS_NET_H
#define CHESS_NET_H

#include <SFML/Graphics.hpp>
#include <memory>
#include "Board.h"

class Net
{
public:

  static Net *getInstance();
  std::array<std::shared_ptr<sf::RectangleShape>, boardsize> operator [](int el);

private:
  static Net *instance;
  Net();

  Net(Net const &n) { this->net = n.net; };

  std::vector<std::array<std::shared_ptr<sf::RectangleShape>, boardsize>> net;

};

#endif //CHESS_NET_H
