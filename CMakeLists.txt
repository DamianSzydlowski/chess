cmake_minimum_required(VERSION 3.13)
project(Chess)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXE_LINKER_FLAGS "-lsfml-graphics -lsfml-window -lsfml-system")

add_executable(Chess main.cpp Board.cpp Pieces/Pieces.h Graphic.cpp Graphic.h exceptions.cpp exceptions.h Pieces/Pawn.cpp Pieces/Pawn.h Pieces/Rook.cpp Pieces/Rook.h Pieces/Knight.cpp Pieces/Knight.h Pieces/Bishop.cpp Pieces/Bishop.h Pieces/Quenn.cpp Pieces/Quenn.h Pieces/King.cpp Pieces/King.h Net.cpp Net.h)

#set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
#find_package(SFML 2 REQUIRED system window graphics network audio)
#if(SFML_FOUND)
#    include_directories(${SFML_INCLUDE_DIR})
#    target_link_libraries(Chess ${SFML_LIBRARIES})
#endif()

target_link_libraries(Chess pthread)

find_package(SFML 2 REQUIRED system window graphics network audio)
if (SFML_FOUND)
    include_directories(${SFML_INCLUDE_DIR})
    target_link_libraries(Chess ${SFML_LIBRARIES} ${SFML_DEPENDENCIES})
endif ()
add_executable(Pieces Pieces/Pieces.h)