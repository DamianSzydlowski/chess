//
// Created by xarti on 18.01.19.
//

#ifndef CHESS_BOARD_H
#define CHESS_BOARD_H



#include <memory>
#include <vector>
#include <array>
#include "Pieces/Pieces.h"

static const int boardsize = 8;


class Board
{
public:
  Board();
  ~Board() = default;

  void Fill();
  void Clear();

  std::array<std::shared_ptr<Pieces>, boardsize> operator [] (int el);

private:
  std::vector<std::array<std::shared_ptr<Pieces>, boardsize>> board;

};

#endif //CHESS_BOARD_H