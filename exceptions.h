//
// Created by xarti on 06.03.19.
//

#ifndef CHESS_EXCEPTIONS_H
#define CHESS_EXCEPTIONS_H

#include <exception>
#include <iostream>

class Exception : public std::exception
{
public:
  explicit Exception(const char* message)
  :msg_(message){}
  explicit Exception(const std::string& message)
  :msg_(message){}

  virtual ~Exception() throw (){}

  virtual const char* what() const throw ()
  {
    return msg_.c_str();
  }

protected:
  std::string msg_;

};

#endif //CHESS_EXCEPTIONS_H
