#include <utility>
#include <iostream>

//
// Created by xarti on 18.02.19.
//

#include "Graphic.h"
#include "Pieces/Pieces.h"

Graphic::Graphic()
    : width(600), hight(600), Window(sf::VideoMode(600, 600, 32), "Chess")
{
  for (int i = 0; i < boardsize; ++i)
  {
    std::array<std::shared_ptr<std::pair<int, int>>, boardsize> temp;
    poss.push_back(std::move(temp));
    //std::array<std::shared_ptr<sf::RectangleShape> , 8> temp2;
    //net.push_back(std::move(temp2));
  }

  if (!texture.loadFromFile("../chessboard.jpg"))
  {
    exit(EXIT_FAILURE);
  }
  texture.setSmooth(true);

  boardsprite.setTexture(texture);
  boardsprite.scale(0.6, 0.6);

  int diff = 495/boardsize;
  for (int i = 0; i < boardsize; ++i)
  {
    for (int j = 0; j < boardsize; ++j)
    {
      poss[i][j] = std::make_shared<std::pair<int, int>>(std::make_pair(62 + diff*i, 62 + diff*j));
      /*net[i][j] = std::make_shared <sf::RectangleShape> ();
      net[i][j]->setSize(sf::Vector2f(60.0f, 60.0f));
      net[i][j]->setPosition(poss[i][j]->first-6 , poss[i][j]->second -5);
      net[i][j]->setFillColor(sf::Color(0,191,255,75));
      net[i][j]->setOutlineThickness(1);*/
    }
  }
}

void Graphic::runWindow()
{
  std::thread t1([=]()
                 {
                   while (Window.isOpen())
                   {
                     sf::Event event{};
                     while (Window.pollEvent(event))
                     {
                       if (event.type == sf::Event::Closed)
                       {
                         Window.close();
                         break;
                       }
                       Window.draw(boardsprite);
                       displayNet(PieceGetter());
                       displayPieces();
                       //PieceGetter();
                       Window.display();
                     }
                   }
                 });
  t1.join();

}

void Graphic::displayPieces()
{
  for (int i = 0; i < boardsize; ++i)
  {
    for (int j = 0; j < boardsize; ++j)
    {
      if ((*holder)[i][j] != nullptr)
      {
        (*holder)[i][j]->display().setPosition(poss[i][j]->first, poss[i][j]->second);
        Window.draw((*holder)[i][j]->display());
      }
    }
  }
}

void Graphic::setHolder(std::shared_ptr<Board> board)
{
  holder = std::move(board);
}

void Graphic::displayNet(std::shared_ptr<std::pair<int, int>> cord)
{
  if (cord != nullptr)
  {
    if ((*holder)[cord->first][cord->second] != nullptr)
    {
      //do{
      switch ((*holder)[cord->first][cord->second]->Type())
      {
      case PAWN:

        std::cout << "Pawn" << std::endl;
        break;

      case ROOK:

        std::cout << "Rook" << std::endl;
        break;

      case KNIGHT:

        std::cout << "Knight" << std::endl;
        break;

      case BISHOP:

        std::cout << "Bishop" << std::endl;
        break;

      case QUENN:

        std::cout << "Quenn" << std::endl;
        break;

      case KING:
      {

        std::cout << "King" << std::endl;
        break;
      default:break;
      }
        //}while ();
      }
    }

  }
}

std::shared_ptr<std::pair<int, int>> Graphic::PieceGetter()
{
  std::shared_ptr<sf::Vector2i> relativePos = mouseHandler();
  if (relativePos != nullptr)
  {
    if (relativePos->x - 62 > 0 && relativePos->y - 62 > 0)
    {
      int diff = 495/boardsize;
      int i = (relativePos->x - 62)/diff;
      int j = (relativePos->y - 62)/diff;
      std::cout << "i: " << i << " j: " << j << std::endl;
      return std::make_shared<std::pair<int, int>>(std::make_pair(i, j));
    } else { return nullptr; }
  } else { return nullptr; }
}

std::shared_ptr<sf::Vector2i> Graphic::mouseHandler()
{
  isHold = false;
  if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !isHold)
  {
    isHold = true;
    if (sf::Mouse::getPosition(Window).x < Window.getSize().x && sf::Mouse::getPosition(Window).y < Window.getSize().y)
    {
      return std::make_shared<sf::Vector2i>(sf::Mouse::getPosition(Window));
    } else return nullptr;
  } else
  {
    isHold = false;
    return nullptr;
  }

}

void Graphic::Sellected()
{

}

