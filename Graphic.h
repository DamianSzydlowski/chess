//
// Created by xarti on 18.02.19.
//

#ifndef CHESS_GRAPHIC_H
#define CHESS_GRAPHIC_H

#include <SFML/Graphics.hpp>
#include <thread>
#include "Board.h"



class Graphic
{
public:
  Graphic();
  void runWindow();
  void displayPieces();
  void setHolder(std::shared_ptr<Board> board);
  void displayNet(std::shared_ptr<std::pair<int,int>> cord);
  void Sellected();
  std::shared_ptr<std::pair<int, int>> PieceGetter();
  std::shared_ptr<sf::Vector2i> mouseHandler();

private:
  std::vector<std::array<std::shared_ptr<std::pair<int,int>>, boardsize>> poss;
  //std::vector<std::array<std::shared_ptr<sf::RectangleShape> , 8>> net;
  short width;
  short hight;

  sf::RenderWindow Window;
  sf::Sprite boardsprite;
  sf::Texture texture;
  std::shared_ptr<Board> holder;
  bool isHold;
  bool isSellected;

};

#endif //CHESS_GRAPHIC_H
