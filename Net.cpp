//
// Created by xarti on 01.04.19.
//

#include "Net.h"

Net *Net::instance = nullptr;

std::array<std::shared_ptr<sf::RectangleShape>, 8> Net::operator [](int el)
{
  return net[el];
}

Net::Net()
{
  for (int i = 0; i < boardsize; ++i)
  {
    std::array<std::shared_ptr<sf::RectangleShape>, boardsize> temp;
    net.push_back(std::move(temp));
  }

  int diff = 495/boardsize;

  for (int i = 0; i < boardsize; ++i)
  {
    for (int j = 0; j < boardsize; ++j)
    {
      net[i][j] = std::make_shared<sf::RectangleShape>();
      net[i][j]->setSize(sf::Vector2f(60.0f, 60.0f));
      net[i][j]->setPosition(62 + diff*i - 6, 62 + diff*j - 6);
      net[i][j]->setFillColor(sf::Color(0, 191, 255, 75));
      net[i][j]->setOutlineThickness(1);
    }
  }
}

Net *Net::getInstance()
{
  if (instance == nullptr)
  {
    instance = new Net();
  }
  return instance;
}

