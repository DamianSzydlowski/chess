//
// Created by xarti on 18.01.19.
//

#include <iostream>
#include "Board.h"
#include "Pieces/Pawn.h"
#include "Pieces/Rook.h"
#include "Pieces/Knight.h"
#include "Pieces/Bishop.h"
#include "Pieces/Quenn.h"
#include "Pieces/King.h"

Board::Board()
{
  //board.reserve(8);
  for (int i = 0; i < boardsize; ++i)
  {
    std::array<std::shared_ptr<Pieces>, boardsize> temp;
    board.push_back(std::move(temp));
  }

}

void Board::Fill()
{
  //Setting White pieces

  for (int i = 0; i < boardsize; ++i)
  {
    board[i][1] = std::make_shared<Pawn>(i, 1, 'W');
  }

  {
    board[0][0] = std::make_shared<Rook>(0, 0, 'W');
    board[7][0] = std::make_shared<Rook>(7, 0, 'W');
  }

  {
    board[1][0] = std::make_shared<Knight>(1, 0, 'W');
    board[6][0] = std::make_shared<Knight>(6, 0, 'W');
  }

  {
    board[2][0] = std::make_shared<Bishop>(2, 0, 'W');
    board[5][0] = std::make_shared<Bishop>(5, 0, 'W');
  }

  {
    board[3][0] = std::make_shared<Quenn>(3, 0, 'W');
    board[4][0] = std::make_shared<King>(4, 0, 'W');
  }
  // Setting Black pieces

  for (int i = 0; i < boardsize; ++i)
  {
    board[i][6] = std::make_shared<Pawn>(i, 6, 'B');
  }

  {
    board[0][7] = std::make_shared<Rook>(0, 7, 'B');
    board[7][7] = std::make_shared<Rook>(7, 7, 'B');
  }

  {
    board[1][7] = std::make_shared<Knight>(1, 7, 'B');
    board[6][7] = std::make_shared<Knight>(6, 7, 'B');
  }

  {
    board[2][7] = std::make_shared<Bishop>(2, 7, 'B');
    board[5][7] = std::make_shared<Bishop>(5, 7, 'B');
  }

  {
    board[3][7] = std::make_shared<Quenn>(3, 7, 'B');
    board[4][7] = std::make_shared<King>(4, 7, 'B');
  }
}

void Board::Clear()
{
  // Clears board
  for(const auto &row: board)
  {
    for(auto x: row)
    {
      x = nullptr;
    }
  }
}

std::array<std::shared_ptr<Pieces>, boardsize> Board::operator [](int el)
{
  return board[el];
}



