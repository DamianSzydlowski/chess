//
// Created by xarti on 28.03.19.
//

#ifndef CHESS_ROOK_H
#define CHESS_ROOK_H

#include "Pieces.h"

class Rook : public Pieces
{
public:
  Rook(int X, int Y, char color);

  std::shared_ptr<std::pair<int, int>> Motion() override;
  void Atack() override;
  void setColor(char) override;
  char getColor() const override;
  sf::Sprite &display() override;
  Piece Type() override;
  std::shared_ptr<std::pair<int, int>> Possition() override;
  bool isSelected() override;
  void Select(bool b) override;
  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

protected:
  int cordX;
  int cordY;
  char Color;
  sf::Texture rookTexture;
  sf::Sprite rookSprite;
  bool Selected;
};

#endif //CHESS_ROOK_H
