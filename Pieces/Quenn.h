//
// Created by xarti on 28.03.19.
//

#ifndef CHESS_QUENN_H
#define CHESS_QUENN_H

#include "Pieces.h"

class Quenn : public Pieces
{
public:
  Quenn(int X, int Y, char color);

  std::shared_ptr<std::pair<int, int>> Motion() override;
  void Atack() override;
  void setColor(char) override;
  std::shared_ptr<std::pair<int, int>> Possition() override;
  char getColor() const override;
  sf::Sprite &display() override;
  Piece Type() override;
  bool isSelected() override;
  void Select(bool b) override;
  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

protected:
  int cordX;
  int cordY;
  char Color;
  sf::Texture quennTexture;
  sf::Sprite quennSprite;
  bool Selected;
};

#endif //CHESS_QUENN_H
