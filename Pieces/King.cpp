//
// Created by xarti on 28.03.19.
//

#include "King.h"
#include "../Net.h"

King::King(int X, int Y, char color)
    : cordX(X), cordY(Y), Color(color), Selected(false)
{
  switch (this->Color)
  {
  case 'W':
    if (!kingTexture.loadFromFile("../pawns/King-white.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  case 'B':
    if (!kingTexture.loadFromFile("../pawns/King-black.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  default:
    //throw
    exit(EXIT_FAILURE);
  }
  kingTexture.setSmooth(true);
  kingSprite.setTexture(kingTexture);
  kingSprite.scale(0.18f, 0.18f);
}

void King::Select(bool b)
{
  Selected = b;
}

bool King::isSelected()
{
  return Selected;
}

void King::setColor(char color)
{
  Color = color;
}

char King::getColor() const
{
  return Color;
}

sf::Sprite &King::display()
{
  return kingSprite;
}

Piece King::Type()
{
  return KING;
}

void King::Atack()
{

}

std::shared_ptr<std::pair<int, int>> King::Possition()
{
  return std::make_shared<std::pair<int, int>>(std::make_pair(cordX, cordY));
}

std::shared_ptr<std::pair<int, int>> King::Motion()
{
  return nullptr;
}

void King::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  target.draw(kingSprite, states);

  if (Selected)
  {
    int X[] = {-1, 0, 1};
    int Y[] = {-1, 0, 1};
    for (int i : X)
    {
      for (int j : Y)
      {
        if (cordX + i >= 0 && cordX + i < boardsize && cordY + j >= 0
            && cordY + j < boardsize)
        {
          target.draw(*(*Net::getInstance())[cordX + i][cordY + j], states);
        }
      }
    }
  }

}


