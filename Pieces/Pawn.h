//
// Created by xarti on 28.03.19.
//

#ifndef CHESS_PAWN_H
#define CHESS_PAWN_H

#include "Pieces.h"

class Pawn : public Pieces
{
public:
  Pawn(int X, int Y, char color);
  Pawn() = default;
  bool isSelected() override;
  void Select(bool b) override;
  std::shared_ptr<std::pair<int, int>> Motion() override;
  void Atack() override;
  void setColor(char) override;
  char getColor() const override;
  sf::Sprite & display() override;
  Piece Type() override;
  std::shared_ptr<std::pair<int, int>> Possition() override;
  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

private:
  int cordX;
  int cordY;
  char Color;
  bool FirstMove;
  sf::Texture pawnTexture;
  sf::Sprite pawnSprite;
  bool Selected;
};

#endif //CHESS_PAWN_H
