//
// Created by xarti on 28.03.19.
//

#include "Knight.h"
#include "../Net.h"

Knight::Knight(int X, int Y, char color)
    : cordX(X), cordY(Y), Color(color), Selected(false)
{
  switch (this->Color)
  {
  case 'W':
    if (!knightTexture.loadFromFile("../pawns/Knight-white.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  case 'B':
    if (!knightTexture.loadFromFile("../pawns/Knight-black.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  default:
    //throw
    exit(EXIT_FAILURE);
  }
  knightTexture.setSmooth(true);
  knightSprite.setTexture(knightTexture);
  knightSprite.scale(0.18f, 0.18f);
}

Piece Knight::Type()
{
  return KNIGHT;
}

void Knight::setColor(char color)
{
  Color = color;
}

char Knight::getColor() const
{
  return Color;
}

sf::Sprite &Knight::display()
{
  return knightSprite;
}

bool Knight::isSelected()
{
  return Selected;
}

void Knight::Select(bool b)
{
  Selected = b;
}

void Knight::Atack()
{

}

std::shared_ptr<std::pair<int, int>> Knight::Motion()
{
  return nullptr;
}

std::shared_ptr<std::pair<int, int>> Knight::Possition()
{
  return std::make_shared<std::pair<int, int>>(std::make_pair(cordX, cordY));
}

void Knight::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  target.draw(knightSprite, states);
  if (Selected)
  {
    int X[] = {2, 1, -1, -2, -2, -1, 1, 2};
    int Y[] = {1, 2, 2, 1, -1, -2, -2, -1};
    for (int i = 0; i < boardsize; ++i)
    {
      if (cordX + Y[i] >= 0 && cordX + Y[i] < boardsize && cordY + X[i] >= 0
          && cordY + X[i] < boardsize)
      {
        target.draw(*(*Net::getInstance())[cordX + Y[i]][cordY + X[i]], states);
      }
    }
  }
}
