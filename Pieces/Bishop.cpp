//
// Created by xarti on 28.03.19.
//

#include "Bishop.h"
#include "../Net.h"

Bishop::Bishop(int X, int Y, char color)
    : cordX(X), cordY(Y), Color(color), Selected(false)
{
  switch (this->Color)
  {
  case 'W':
    if (!bishopTexture.loadFromFile("../pawns/Bishop-white.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  case 'B':
    if (!bishopTexture.loadFromFile("../pawns/Bishop-black.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  default:
    //throw
    exit(EXIT_FAILURE);
  }
  bishopTexture.setSmooth(true);
  bishopSprite.setTexture(bishopTexture);
  bishopSprite.scale(0.18f, 0.18f);
}

void Bishop::setColor(char color)
{
  Color = color;
}

char Bishop::getColor() const
{
  return Color;
}

sf::Sprite &Bishop::display()
{
  return bishopSprite;
}

Piece Bishop::Type()
{
  return BISHOP;
}

bool Bishop::isSelected()
{
  return Selected;
}

void Bishop::Atack()
{

}

void Bishop::Select(bool b)
{
  Selected = b;
}

std::shared_ptr<std::pair<int, int>> Bishop::Possition()
{
  return std::make_shared<std::pair<int, int>>(std::make_pair(cordX, cordY));

}

std::shared_ptr<std::pair<int, int>> Bishop::Motion()
{
  return nullptr;
}

void Bishop::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  target.draw(bishopSprite, states);
  if (Selected)
  {
    for (int i = cordX, j = cordY; i < boardsize && j < boardsize; ++i, ++j)
    {
      target.draw(*(*Net::getInstance())[i][j], states);
    }
    for (int i = cordX, j = cordY; i >= 0 && j < boardsize; --i, ++j)
    {
      target.draw(*(*Net::getInstance())[i][j], states);
    }
    for (int i = cordX, j = cordY; i < boardsize && j >= 0; ++i, --j)
    {
      target.draw(*(*Net::getInstance())[i][j], states);
    }
    for (int i = cordX, j = cordY; i >= 0 && j >= 0; --i, --j)
    {
      target.draw(*(*Net::getInstance())[i][j], states);
    }
  }
}
