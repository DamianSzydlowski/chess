//
// Created by xarti on 18.01.19.
//

#ifndef CHESS_PIECES_H
#define CHESS_PIECES_H

#include <SFML/Graphics.hpp>
#include <memory>

enum Piece{
  PAWN, ROOK, KNIGHT, BISHOP, QUENN, KING
};

class Pieces : public sf::Drawable
{
public:
  Pieces() = default;
  virtual std::shared_ptr<std::pair<int, int>> Motion() = 0;
  virtual void Atack() = 0;
  virtual void setColor(char) = 0;
  virtual char getColor() const = 0;
  virtual sf::Sprite & display() = 0;
  virtual Piece Type() = 0;
  virtual std::shared_ptr<std::pair<int, int>> Possition()=0;
  virtual bool isSelected()=0;
  virtual void Select(bool)=0;
  virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
};

#endif //CHESS_PIECES_H
