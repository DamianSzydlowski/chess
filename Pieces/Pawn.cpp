//
// Created by xarti on 28.03.19.
//

#include "Pawn.h"
#include "../Net.h"

Pawn::Pawn(int X, int Y, char color)
    : cordX(X), cordY(Y), Color(color), FirstMove(true), Selected(false)
{
  switch (this->Color)
  {
  case 'W':
    if (!pawnTexture.loadFromFile("../pawns/Pawn-white.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  case 'B':
    if (!pawnTexture.loadFromFile("../pawns/Pawn-black.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  default:
    //throw
    exit(EXIT_FAILURE);
  }
  pawnTexture.setSmooth(true);
  pawnSprite.setTexture(pawnTexture);
  pawnSprite.scale(0.18f, 0.18f);
}

Piece Pawn::Type()
{
  return PAWN;
}

sf::Sprite &Pawn::display()
{
  return pawnSprite;
}

void Pawn::setColor(char color)
{
  Color = color;
}

char Pawn::getColor() const
{
  return Color;
}

void Pawn::Select(bool b)
{
  Selected = b;
}

bool Pawn::isSelected()
{
  return Selected;
}

void Pawn::Atack()
{

}

std::shared_ptr<std::pair<int, int>> Pawn::Motion()
{
  if (FirstMove)
  {
    return std::make_shared<std::pair<int, int>>(std::make_pair(0, 2));
  } else { return std::make_shared<std::pair<int, int>>(std::make_pair(0, 1)); }
}

std::shared_ptr<std::pair<int, int>> Pawn::Possition()
{
  return std::make_shared<std::pair<int, int>>(std::make_pair(cordX, cordY));
}

void Pawn::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  target.draw(pawnSprite, states);
  if (Selected)
  {
    if (Color == 'W')
    {
      for (int i = cordY; i <= cordY + FirstMove ? 2 : 1; ++i)
      {
        target.draw(*(*Net::getInstance())[i][cordY], states);
      }
    } else
    {
      for (int i = cordY; i >= cordY - FirstMove ? 2 : 1; --i)
      {
        target.draw(*(*Net::getInstance())[i][cordY], states);
      }
    }
  }
}

