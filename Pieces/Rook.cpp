//
// Created by xarti on 28.03.19.
//

#include "Rook.h"
#include "../Net.h"

Rook::Rook(int X, int Y, char color)
    : cordX(X), cordY(Y), Color(color), Selected(false)
{
  switch (this->Color)
  {
  case 'W':
    if (!rookTexture.loadFromFile("../pawns/Rook-white.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  case 'B':
    if (!rookTexture.loadFromFile("../pawns/Rook-black.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  default:
    //throw
    exit(EXIT_FAILURE);
  }
  rookTexture.setSmooth(true);
  rookSprite.setTexture(rookTexture);
  rookSprite.scale(0.18f, 0.18f);
}

Piece Rook::Type()
{
  return ROOK;
}

void Rook::setColor(char color)
{
  Color = color;
}

char Rook::getColor() const
{
  return Color;
}

sf::Sprite &Rook::display()
{
  return rookSprite;
}

bool Rook::isSelected()
{
  return Selected;
}

void Rook::Select(bool b)
{
  Selected = b;
}

void Rook::Atack()
{

}

std::shared_ptr<std::pair<int, int>> Rook::Possition()
{
  return std::make_shared<std::pair<int, int>>(std::make_pair(cordX, cordY));
}

std::shared_ptr<std::pair<int, int>> Rook::Motion()
{
  return nullptr;
}

void Rook::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  target.draw(rookSprite, states);
  if (Selected)
  {
    auto net = Net::getInstance();
    for (int i = 0; i < boardsize; i++)
    {
      target.draw(*(*net)[cordX][i], states);
    }
    for (int j = 0; j < boardsize; j++)
    {
      target.draw(*(*net)[j][cordY], states);
    }
  }
}
