//
// Created by xarti on 28.03.19.
//

#ifndef CHESS_KING_H
#define CHESS_KING_H

#include "Pieces.h"

class King : public Pieces
{
public:
  King(int X, int Y, char color);

  std::shared_ptr<std::pair<int, int>> Motion() override;
  void Atack() override;
  void setColor(char) override;
  char getColor() const override;
  sf::Sprite &display() override;
  Piece Type() override;
  std::shared_ptr<std::pair<int, int>> Possition() override;
  bool isSelected() override;
  void Select(bool b) override;
  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

protected:
  int cordX;
  int cordY;
  char Color;
  sf::Texture kingTexture;
  sf::Sprite kingSprite;
  bool Selected;
};

#endif //CHESS_KING_H
