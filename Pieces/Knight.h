//
// Created by xarti on 28.03.19.
//

#ifndef CHESS_KNIGHT_H
#define CHESS_KNIGHT_H

#include "Pieces.h"

class Knight : public Pieces
{
public:
  Knight(int X, int Y, char color);

  std::shared_ptr<std::pair<int, int>> Motion() override;
  char getColor() const override;
  void Atack() override;
  void setColor(char) override;
  sf::Sprite &display() override;
  std::shared_ptr<std::pair<int, int>> Possition() override;
  Piece Type() override;
  bool isSelected() override;
  void Select(bool b) override;
  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

protected:
  int cordX;
  int cordY;
  char Color;
  sf::Texture knightTexture;
  sf::Sprite knightSprite;
  bool Selected;
};

#endif //CHESS_KNIGHT_H
