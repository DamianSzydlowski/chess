//
// Created by xarti on 28.03.19.
//

#include "Quenn.h"
#include "../Net.h"

Quenn::Quenn(int X, int Y, char color)
    : cordX(X), cordY(Y), Color(color), Selected(false)
{
  switch (this->Color)
  {
  case 'W':
    if (!quennTexture.loadFromFile("../pawns/Quenn-white.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  case 'B':
    if (!quennTexture.loadFromFile("../pawns/Quenn-black.png"))
    {
      exit(EXIT_FAILURE);
    }
    break;

  default:
    //throw
    exit(EXIT_FAILURE);
  }
  quennTexture.setSmooth(true);
  quennSprite.setTexture(quennTexture);
  quennSprite.scale(0.18f, 0.18f);
}

void Quenn::setColor(char color)
{
  Color = color;
}

char Quenn::getColor() const
{
  return Color;
}

sf::Sprite &Quenn::display()
{
  return quennSprite;
}

Piece Quenn::Type()
{
  return QUENN;
}

bool Quenn::isSelected()
{
  return Selected;
}

void Quenn::Select(bool b)
{
  Selected = b;
}

void Quenn::Atack()
{

}

std::shared_ptr<std::pair<int, int>> Quenn::Possition()
{
  return std::make_shared<std::pair<int, int>>(std::make_pair(cordX, cordY));
}

std::shared_ptr<std::pair<int, int>> Quenn::Motion()
{
  return nullptr;
}

void Quenn::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
  target.draw(quennSprite, states);
  if (Selected)
  {
    for (int i = 0; i < boardsize; i++)
    {
      target.draw(*(*Net::getInstance())[cordX][i], states);
    }
    for (int j = 0; j < boardsize; j++)
    {
      target.draw(*(*Net::getInstance())[j][cordY], states);
    }
    for (int i = cordX, j = cordY; i < boardsize && j < boardsize; ++i, ++j)
    {
      target.draw(*(*Net::getInstance())[i][j], states);
    }
    for (int i = cordX, j = cordY; i >= 0 && j < boardsize; --i, ++j)
    {
      target.draw(*(*Net::getInstance())[i][j], states);
    }
    for (int i = cordX, j = cordY; i < boardsize && j >= 0; ++i, --j)
    {
      target.draw(*(*Net::getInstance())[i][j], states);
    }
    for (int i = cordX, j = cordY; i >= 0 && j >= 0; --i, --j)
    {
      target.draw(*(*Net::getInstance())[i][j], states);
    }
  }
}
